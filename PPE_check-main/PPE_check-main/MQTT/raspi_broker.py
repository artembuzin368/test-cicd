# import paho.mqtt.client as mqtt

# def on_message(client, userdata, message):
#     print(message.topic, message.payload)


# Define the username and password for the connection
username = "ilya-jetson"
password = "jetson-ai"


# client = mqtt.Client()
# client.on_message = on_message
# client.connect("localhost", 1883)
# client.subscribe("jetson/temperature")
# client.loop_forever()


import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # Subscribe to the MQTT topic
    client.subscribe("jetson/temperature")

def on_message(client, userdata, msg):
    print("Received message on topic " + msg.topic + " with payload " + str(msg.payload))

# Create an MQTT client instance
client = mqtt.Client()

# Set the authentication details
client.username_pw_set(username, password)

# Set up the callback functions
client.on_connect = on_connect
client.on_message = on_message

# Connect to the MQTT broker running on the same Raspberry Pi
client.connect("localhost", 1883)

# Start the MQTT client loop to listen for incoming messages
client.loop_forever()
