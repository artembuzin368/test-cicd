import paho.mqtt.publish as publish
import random
import time


# Define the username and password for the connection
username = "ilya-jetson"
password = "jetson-ai"


while True:
    temperature = random.randint(20, 30)
    publish.single("jetson/temperature", temperature, hostname="192.168.0.42", auth={'username':username, 'password':password})
    time.sleep(1)
