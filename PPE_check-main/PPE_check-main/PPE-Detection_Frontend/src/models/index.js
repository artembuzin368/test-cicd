// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';

const State = {
  "MASK": "MASK",
  "GLOVES": "GLOVES",
  "APRON": "APRON"
};

const { Alert, Client } = initSchema(schema);

export {
  Alert,
  Client,
  State
};