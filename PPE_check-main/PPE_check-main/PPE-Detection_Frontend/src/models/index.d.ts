import { ModelInit, MutableModel, __modelMeta__, ManagedIdentifier } from "@aws-amplify/datastore";
// @ts-ignore
import { LazyLoading, LazyLoadingDisabled, AsyncItem, AsyncCollection } from "@aws-amplify/datastore";

export enum State {
  MASK = "MASK",
  GLOVES = "GLOVES",
  APRON = "APRON"
}



type EagerAlert = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Alert, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly timestamp: number;
  readonly ppeState: State[] | keyof typeof State;
  readonly clientID?: string | null;
  readonly Client?: Client | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyAlert = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Alert, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly timestamp: number;
  readonly ppeState: State[] | keyof typeof State;
  readonly clientID?: string | null;
  readonly Client: AsyncItem<Client | undefined>;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type Alert = LazyLoading extends LazyLoadingDisabled ? EagerAlert : LazyAlert

export declare const Alert: (new (init: ModelInit<Alert>) => Alert) & {
  copyOf(source: Alert, mutator: (draft: MutableModel<Alert>) => MutableModel<Alert> | void): Alert;
}

type EagerClient = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Client, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly Alerts?: (Alert | null)[] | null;
  readonly station_name?: string | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyClient = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Client, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly Alerts: AsyncCollection<Alert>;
  readonly station_name?: string | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type Client = LazyLoading extends LazyLoadingDisabled ? EagerClient : LazyClient

export declare const Client: (new (init: ModelInit<Client>) => Client) & {
  copyOf(source: Client, mutator: (draft: MutableModel<Client>) => MutableModel<Client> | void): Client;
}