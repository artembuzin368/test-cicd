import './App.css';
import { withAuthenticator } from '@aws-amplify/ui-react';
import { DataStore } from '@aws-amplify/datastore';
import { Alert, Client, State } from './models';
import React, { useState, useEffect } from 'react';
import TableChart from './components/Table';
import { Link } from 'react-router-dom';

import { Amplify } from 'aws-amplify';
import awsconfig from "./aws-exports";


function App({ signOut, user }) {
  const [client, setClient]= useState([]);
  const [alert, setAlert]=useState([]);
  useEffect(() => {
    const subscription = DataStore.observe(Alert).subscribe((msg) => {
      console.log(msg.element);
      setAlert(msg.element);
    });
    fetchClient();
    return () => subscription.unsubscribe();
  }, []);
  async function fetchClient(){
    const client = await DataStore.query(Client);
    const clientsFromApi =  client[0];
    console.log(clientsFromApi);
    setClient(clientsFromApi);
  }
  async function createAlert(event){
    event.preventDefault();
    
    let alert = await DataStore.save(
      new Alert({
      "timestamp": 1023123,
      "ppeState":  State.GLOVES,
      "Client": client,
      "clientID": client.id
    }));
    console.log(alert);
    setAlert(alert);
    // event.target.reset();
  }
  async function createClient(){
    
    let client = await DataStore.save(
      new Client({
      "Alerts": [],
      "station_name": "factory"
    })
  );
    console.log(client);
    setClient(client);
    // event.target.reset();
  }
  async function fetchAlerts(){
    const alerts = await DataStore.query(Alert);
    const alertsFromApi = alerts;
    console.log("hello");
    console.log(alertsFromApi);
    if (alertsFromApi==null){
      setAlert(alertsFromApi);
    }
    
  }


  return (

    <div className="grid-container">
      <header className="header">
        <div className="btn">
          <button className="btn" onClick={signOut}>Sign Out</button>
        </div>
        <div className="vl"></div>
      </header>

      <div className="left-grid">
        <div className="tab">
          <button className="tablinks">Overview</button>
          <button className="tablinks">Station Viewer</button>
          <button className="tablinks">Devices</button>
          <button className="tablinks">Log</button>
          <button className="tablinks" onClick={createAlert}>Alerts</button>
          <button className="tablinks" onClick={fetchAlerts}>Query Alert</button>
          <button className="tablinks" onClick={createClient}>Client</button>
          <button className="tablinks" onClick={fetchClient}>Query Client</button>
          <hr />
          <Link to="/Profile">
            <button className="tablinks">Profile</button>
          </Link>
        </div>
      </div>

      <div className="right-grid">
        <div className="tabcontent">
          <h1>Alerts</h1>
        </div>
        <TableChart />
      </div>
    </div>
  );
}

export default withAuthenticator(App);
