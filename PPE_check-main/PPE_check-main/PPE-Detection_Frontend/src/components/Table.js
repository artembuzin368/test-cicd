import React, { useState } from 'react';
import './Table.css';

const TableChart = () => {
  const [inputs, setInputs] = useState({
    column1: '',
    column2: '',
    column3: '',
  });
  const [rows, setRows] = useState([]);

  const handleInputChange = (event) => {
    setInputs({
      ...inputs,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setRows([...rows, inputs]);
    setInputs({ column1: '', column2: '', column3: '' });
  };

  return (
    <div className="table-chart">
      <form className="form" onSubmit={handleSubmit}>
        <input
          className="input"
          type="text"
          name="column1"
          value={inputs.column1}
          onChange={handleInputChange}
        />
        <input
          className="input"
          type="text"
          name="column2"
          value={inputs.column2}
          onChange={handleInputChange}
        />
        <input
          className="input"
          type="text"
          name="column3"
          value={inputs.column3}
          onChange={handleInputChange}
        />
        <button className="button" type="submit">Add Row</button>
      </form>
      <table className="table">
        <thead className="table-head">
          <tr>
            <th className="table-head-col">Time</th>
            <th className="table-head-col">Location</th>
            <th className="table-head-col">Alert</th>
          </tr>
        </thead>
        <tbody className="table-body">
          {rows.map((row, index) => (
            <tr key={index} className="table-row">
              <td className="table-col">{row.column1}</td>
              <td className="table-col">{row.column2}</td>
              <td className="table-col">{row.column3}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableChart;
