import './App.css';
import { withAuthenticator } from '@aws-amplify/ui-react';
import { DataStore } from '@aws-amplify/datastore';
import { Alert } from './models';
import React, { useState, useEffect } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { onCreateAlert } from './graphql/subscriptions';
import { Link } from 'react-router-dom';

function App({ signOut, user }) {
  return (

    <div className="grid-container">
      <header className="header">
        <div className="btn">
          <button className="btn" onClick={signOut}>Sign Out</button>
        </div>
        <div className="vl"></div>
      </header>

      <div className="left-grid">
        <div className="tab">
          <Link to="/">
            <button>Home</button>
          </Link>
        </div>
      </div>

      <div className="right-grid">
      <div className="tabcontent">
          <h1>Profile</h1>
        </div>
        <div className="profile-content">
         <p id="profile-text"> Name: {user.attributes.name}</p>
          <p id="profile-text"> Email: {user.attributes.email}</p>
        </div>
      </div>
    </div>
  );
}

export default withAuthenticator(App);
